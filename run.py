from core import MainClass
from datetime import datetime
from time import sleep

version = "version 1.0.0"
print(version)

while True:
    minuto, secondo = datetime.now().strftime('%M')
    if int(minuto) == 3 or int(minuto) == 0:
        MainClass()
        sleep(60*10)
    else:
        sleep(60)
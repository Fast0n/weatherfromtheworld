from PIL import Image, ImageDraw, ImageFont
from InstagramAPI import InstagramAPI
from settings import user, pwd, API_key, caption_a, caption_b, caption_c
import requests
from datetime import datetime
import random
import json

InstagramAPI = InstagramAPI(user, pwd)

def MainClass():
    try:
        name, temp, description = getWeather()

        makePic(datetime.now().strftime('%H:%M'),
                str(name) + ", " + str(temp) + " °C\n"+str(description), "test.jpg", (26, 26, 26), (192, 192, 192))

        publish(caption_a.format(name.replace(" ", ""), temp, description), "test.jpg")
    except TypeError:
        MainClass()


def makePic(time, desc, nameFile, colorT, colorD):
    W, H = (1000, 1000)
    im = Image.new("RGB", (W, H), colorT)
    MAX_W, MAX_H = im.size
    draw = ImageDraw.Draw(im)

    font = ImageFont.truetype('./assets/ProductSansBold.ttf', 150)
    w, h = draw.textsize(time, font=font)
    draw.text(((MAX_W - w) / 2, (MAX_H - h) / 4), time, colorD, font=font)

    if int(len(desc)) > 35:
        sizeText = 60
    else:
        sizeText = 100

    font1 = ImageFont.truetype('./assets/ProductSans.ttf', sizeText)
    w1, h1 = draw.textsize(desc, font=font1)
    draw.text(((MAX_W - w1) / 2, (MAX_H - h1) / 2),
                  desc, colorD, font=font1)
    im.save(nameFile)


def publish(caption, nameFile):
    InstagramAPI.login()
    photo_path = nameFile
    InstagramAPI.uploadPhoto(photo_path, caption=caption)


def getWeather():
    zipcode = str(random.randint(10000, 99999))
    base_url = "http://api.openweathermap.org/data/2.5/weather"
    Final_url = base_url + "?zip=" + zipcode + \
        "&units=metric" + "&lang=it" + "&appid=" + API_key

    weather_data = requests.get(Final_url).json()

    try:
        name = weather_data["name"]
        temp = str(int(weather_data["main"]["temp"]))
        description = str(weather_data["weather"]
                          [0]["description"]).capitalize()

        return name, temp, description
    except KeyError:
        getWeather()

